# Memoria Técnica - Proyecto de Baloncesto

## Índice
1. [Introducción](#introducción)
2. [Descripción del Proyecto](#descripción-del-proyecto)
3. [Pipeline de Integración Continua](#pipeline-de-integración-continua)
4. [Fase de "release" y Despliegue en Pre-Producción](#fase-de-release-y-despliegue-en-pre-producción)
5. [Nuevo Sprint y Issues](#nuevo-sprint-y-issues)
6. [Programación de las ramas](#programación-de-las-ramas)

## 1. Introducción
Esta memoria técnica describe las actividades realizadas en el proyecto de Baloncesto, detallando los cambios y requisitos implementados en el mismo. También se incluyen los pasos seguidos para la integración continua, despliegue en pre-producción y los nuevos issues asignados en un sprint.

## 2. Descripción del Proyecto
El proyecto de Baloncesto se basa en una aplicación web que permite a los usuarios votar por el mejor jugador de la liga ACB. La aplicación está desarrollada utilizando tecnologías como Java, JSP (JavaServer Pages), Maven y MySQL para la persistencia de datos.

## 3. Pipeline de Integración Continua
El pipeline de integración continua se ha configurado de la siguiente manera:

- **Stages**:
  - build
  - test
  - qa
  - release
  - deploy

- **Empaquetar** (Stage: build):
  - Realiza el empaquetado del proyecto utilizando Maven.
  - Genera los archivos necesarios para el despliegue.

- **Pruebas Unitarias** (Stage: build):
  - Ejecuta las pruebas unitarias utilizando el framework de pruebas de Maven.

- **Pruebas Funcionales** (Stage: test):
  - Realiza las pruebas funcionales de la aplicación.
  - Verifica el funcionamiento correcto de las características principales.

- **Calidad de Código** (Stage: qa):
  - Analiza el código fuente utilizando SonarQube para garantizar la calidad.
  - Verifica que el proyecto cumple con los estándares de calidad definidos.

- **Despliegue en Pre-Producción** (Stage: release):
  - Realiza el despliegue de la aplicación en un entorno de pre-producción.
  - Utiliza una imagen de contenedor con el nombre "baloncesto-pre".

- **Despliegue en Producción** (Stage: deploy):
  - Realiza el despliegue de la aplicación en producción.
  - Utiliza una imagen de contenedor con el nombre "baloncesto".

## 4. Fase de "release" y Despliegue en Pre-Producción
Se ha añadido una nueva fase llamada "release" en el pipeline, que se encuentra entre las fases "qa" y "deploy". Esta fase incluye un trabajo llamado "despliegue-pre", que se encarga de desplegar la aplicación en un entorno de pre-producción. Se utiliza una imagen de contenedor con el nombre "baloncesto-pre" para este despliegue.

## 5. Nuevo Sprint y Issues
Se ha creado un nuevo sprint (milestone) en el proyecto, y se han asignado los siguientes issues:

- **REQ-1: Restablecer los votos de todos los jugadores**:
  - Como nuevo requisito de la aplicación, se debe agregar un botón en la página principal para restablecer los votos de todos los jugadores en la base de datos.

- **REQ-2: Mostrar los votos de los jugadores en una nueva página**:
  - Como nuevo requisito de la aplicación, se debe agregar un botón en la página principal para mostrar una nueva página (por ejemplo, VerVotos.jsp) que muestre una tabla o lista con los nombres de todos los jugadores y sus votos.

- **PF-A: Prueba funcional para restablecer los votos y verificar en la página**:
  - Se debe programar una nueva prueba funcional en el archivo PruebasPhantomjsIT.java.
  - Esta prueba debe simular la pulsación del botón "Poner votos a cero" en la página principal, luego la pulsación del botón "Ver votos", y finalmente verificar que todos los votos en la página VerVotos.jsp sean cero.

- **PF-B: Prueba funcional para votar por un nuevo jugador y verificar en la página**:
  - Se debe programar una nueva prueba funcional en el archivo PruebasPhantomjsIT.java.
  - Esta prueba debe introducir el nombre de un nuevo jugador en la página principal, seleccionar la opción "Otro", pulsar el botón "Votar", volver a la página principal, simular la pulsación del botón "Ver votos" y verificar que el nuevo jugador tenga 1 voto en la página VerVotos.jsp.

- **PU: Caso de prueba para el método actualizarJugador()**:
  - Se debe programar un caso de prueba en el archivo ModeloDatosTest.java.
  - Este caso de prueba debe simular una base de datos de prueba y comprobar que el método actualizarJugador() incrementa en 1 los votos del jugador.

- **QA: Requisito de calidad utilizando SonarQube**:
  - Como requisito de calidad, se debe asegurar que el código fuente completo del proyecto tenga un límite de 20 problemas importantes (major issues) según SonarQube.
  - En caso de superar este límite, el despliegue de la aplicación no será posible en producción.

## 6. Programación de las ramas
Para los issues REQ-1 y PU, se deben crear ramas separadas:

- **Rama para el requisito REQ-1 y prueba unitaria PU**:
  - Se creará una rama específica para el desarrollo del requisito REQ-1 y la implementación de la prueba unitaria PU.
  - Se realizarán los cambios necesarios en los archivos correspondientes.
  - Al finalizar, se realizará la integración de la rama en la rama principal del proyecto.

Nota: El detalle completo de los cambios realizados en los archivos se encuentra en el repositorio del proyecto.

Esta memoria técnica resume las principales acciones llevadas a cabo en el proyecto de Baloncesto, incluyendo la implementación de nuevas funcionalidades, la configuración del pipeline de integración continua, el despliegue en pre-producción y la programación de los issues asignados en un sprint.
