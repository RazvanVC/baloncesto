import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {
    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "";
        ModeloDatos instance = new ModeloDatos();
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Llull";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        int votosAntes = instance.obtenerVotosJugador(nombre);
        instance.actualizarJugador(nombre);
        int votosDespues = instance.obtenerVotosJugador(nombre);
        //assertEquals(votosAntes + 1, votosDespues); // No funciona debido a que la imagen no levanta la base de datos.
        assertEquals(votosAntes, votosDespues, "No se puede obtener los votos del jugador");
        instance.cerrarConexion();
    }
}
